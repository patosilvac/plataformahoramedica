﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="agendamiento.aspx.cs" Inherits="PlataformaHoraMedica.agendamiento" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="registro-form container-fluid col-4">
        <h3 class="text-center text-primary">Agendamiento</h3>
        <div class="form-group">
            <asp:Label ID="Label3" runat="server" Text="Rut"></asp:Label>
            <asp:TextBox ID="txtRutAge" runat="server" CssClass="form-control" ReadOnly="True">15283505-1</asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblNombreAge" runat="server" Text="Nombre"></asp:Label>
            <asp:TextBox ID="txtNombreAge" runat="server" CssClass="form-control" ReadOnly="True">Patricio</asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblApellidoReg" runat="server" Text="Apellido"></asp:Label>
            <asp:TextBox ID="txtApellidoAge" runat="server" CssClass="form-control" ReadOnly="True">Silva</asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblEspAge" runat="server" Text="Especialidad"></asp:Label>
            <asp:DropDownList ID="listadoEspeAge" runat="server" CssClass="form-control">
                <asp:ListItem Value="0">Seleccionar</asp:ListItem>
                <asp:ListItem Value="1">Neurologia</asp:ListItem>
                <asp:ListItem Value="2">Oftalmologia</asp:ListItem>
                <asp:ListItem Value="3">Traumatologia</asp:ListItem>
                <asp:ListItem Value="4">Otorrinolaringologia</asp:ListItem>
                <asp:ListItem Value="5">Pediatria</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label ID="LblMedicoAge" runat="server" Text="Medico"></asp:Label>
            <asp:DropDownList ID="listadoMedAge" runat="server" CssClass="form-control">
                <asp:ListItem Value="0">Seleccionar</asp:ListItem>
                <asp:ListItem Value="1">Dr. Silva</asp:ListItem>
                <asp:ListItem Value="2">Dr. Aceituno</asp:ListItem>
                <asp:ListItem Value="3">Dr. Quintana</asp:ListItem>
                <asp:ListItem Value="4">Dra. Alvarez</asp:ListItem>
                <asp:ListItem Value="5">Dr. Perez</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label ID="labelError" runat="server" Text="" CssClass="text-danger"></asp:Label>
        </div>
        <div class="form-group">
            <asp:Button ID="btnHorasDisp" runat="server" Text="Ver Horas disponibles" CssClass="btn btn-primary btn-block" OnClick="Button1_Click" />
        </div>
        <div class="form-group">
            <asp:Button ID="btnHorasAgen" runat="server" Text="Mis Horas agendadas" CssClass="btn btn-success btn-block" OnClick="Button2_Click" />
        </div>
    </div>
</asp:Content>

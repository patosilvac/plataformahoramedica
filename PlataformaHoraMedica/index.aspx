﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="index.aspx.cs" Inherits="PlataformaHoraMedica.index" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <div class="login-form container-fluid col-3">
        <h1 class="text-center text-primary">PHM</h1>
        <h3 class="text-center text-primary">Inicio de Sesion</h3>
        <div class="form-group">
            <asp:Label ID="LblRut" runat="server" Text="Rut"></asp:Label>
            <asp:TextBox ID="TxtRut" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblPass" runat="server" Text="Contraseña"></asp:Label>
            <asp:TextBox ID="TxtPass" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="labelError" runat="server" Text="" CssClass="text-danger"></asp:Label>
        </div>
        <div class="form-group">
            <asp:Button ID="btnLogin" runat="server" Text="Ingresar" OnClick="Button1_Click" CssClass="btn btn-primary btn-block" />
        </div>
        
        <div class="text-center">
            <asp:LinkButton ID="LinkRegistro" runat="server" OnClick="LinkButton1_Click" CssClass="btn btn-success btn-block">Registrar</asp:LinkButton>
        </div>
    </div>
    
    
    
    
    
    

</asp:Content>

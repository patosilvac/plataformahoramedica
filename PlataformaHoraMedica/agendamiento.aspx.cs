﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlataformaHoraMedica
{
    public partial class agendamiento : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string rut, nombre, apellido, especialidad, medico, msjError="";
            rut = txtRutAge.Text;
            nombre = txtNombreAge.Text;
            apellido = txtApellidoAge.Text;
            especialidad = listadoEspeAge.SelectedItem.Text;
            medico = listadoMedAge.SelectedItem.Text;

            if (rut == "" || nombre == "" || apellido == "" || especialidad == "Seleccionar" || medico == "Seleccionar")
            {
                msjError = "Faltan campos por llenar";
                labelError.Text = msjError;
            }
            else
            {
                Response.Redirect("horas_disponibles.aspx");
            }

            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("horas_agendadas.aspx");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlataformaHoraMedica
{
    public partial class horas_disponibles : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string horario, msjError="";

            horario = RadioButtonList1.SelectedValue;

            if (horario == "")
            {
                msjError = "Debe seleccionar un horario";
                labelError.Text = msjError;
            }
            else
            {
                Response.Redirect("agendamiento.aspx");
            }

            
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("agendamiento.aspx");
        }
    }
}
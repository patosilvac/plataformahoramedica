﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="horas_agendadas.aspx.cs" Inherits="PlataformaHoraMedica.horas_agendadas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="agendamiento container-fluid col-8">
        <h3 class="text-center text-primary">Horas Agendadas</h3>
        <div class="form-group">
            <asp:Label ID="LblRutHA" runat="server" Text="Rut"></asp:Label>
            <asp:TextBox ID="txtRutHA" runat="server" CssClass="form-control" ReadOnly="True">15283505-1</asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblNombreHA" runat="server" Text="Nombre"></asp:Label>
            <asp:TextBox ID="txtNombreHA" runat="server" CssClass="form-control" ReadOnly="True">Patricio</asp:TextBox>
        </div>
     </div>
    <div class="container-fluid col-8">
        <div class="form-group">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">Fecha</th>
                  <th scope="col">Horario</th>
                  <th scope="col">Medico</th>
                  <th scope="col">Especialidad</th>
                  <th scope="col">Confirmar</th>
                  <th scope="col">Elimnar</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>24/11/2020</td>
                  <td>9:00</td>
                  <td>Dr. Silva</td>
                  <td>Neurologia</td>
                  <td>
                      <asp:Button ID="btnConfirmar" runat="server" Text="Confirmar" cssClass="btn btn-success"/>
                  </td>
                  <td>
                      <asp:Button ID="btnEliminar" runat="server" Text="Eliminar" cssClass="btn btn-danger"/>
                  </td>
                </tr>
                
              </tbody>
            </table>
            
        </div>
        </div>
        <div class="agendamiento container-fluid col-4">
            <div class="form-group">
                <asp:Button ID="btnVolverAge" runat="server" Text="Volver" CssClass="btn btn-warning btn-block" OnClick="Button2_Click" />
            </div>
         </div>
        
    
</asp:Content>

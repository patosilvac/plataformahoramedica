﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site1.Master" AutoEventWireup="true" CodeBehind="registrar.aspx.cs" Inherits="PlataformaHoraMedica.registrar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="registro-form container-fluid col-4">
        <h1 class="text-center text-primary">PHM</h1>
        <h3 class="text-center text-primary">Registro de Pacientes</h3>
        <p class="text-center text-primary">Todos los campos son obligatorios</p>
        <div class="form-group">
            <asp:Label ID="LblRutReg" runat="server" Text="Rut"></asp:Label>
            <asp:TextBox ID="txtRutReg" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblNombreReg" runat="server" Text="Nombre"></asp:Label>
            <asp:TextBox ID="txtNombreReg" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblApellidoReg" runat="server" Text="Apellido"></asp:Label>
            <asp:TextBox ID="txtApellidoReg" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblComunaReg" runat="server" Text="Comuna"></asp:Label>
            <asp:DropDownList ID="ListadoComunasReg" runat="server" CssClass="form-control">
                <asp:ListItem Value="0">Seleccionar</asp:ListItem>
                <asp:ListItem Value="1">Macul</asp:ListItem>
                <asp:ListItem Value="2">Puente Alto</asp:ListItem>
                <asp:ListItem Value="3">Santiago</asp:ListItem>
                <asp:ListItem Value="4">Providencia</asp:ListItem>
                <asp:ListItem Value="5">Maipu</asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="form-group">
            <asp:Label ID="LblDireccionReg" runat="server" Text="Direccion"></asp:Label>
            <asp:TextBox ID="txtDireccionReg" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblNumeroReg" runat="server" Text="Numero"></asp:Label>
            <asp:TextBox ID="txtNumeroReg" runat="server" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblPassReg" runat="server" Text="Contraseña"></asp:Label>
            <asp:TextBox ID="txtPassReg" runat="server" CssClass="form-control" TextMode="Password"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="labelError" runat="server" Text="" CssClass="text-danger"></asp:Label>
        </div>
        <div class="form-group">
            <asp:Label ID="labelResumen" runat="server" Text="" CssClass="text-primary"></asp:Label>
        </div>
        <div class="form-group">
            <asp:Button ID="btnReg" runat="server" Text="Registrar" CssClass="btn btn-success btn-block" OnClick="btnReg_Click" />
        </div>
        <div class="text-center">
            <asp:LinkButton ID="linkVolverLogin" runat="server" OnClick="LinkButton1_Click" CssClass="btn btn-warning btn-block">Volver</asp:LinkButton>
        </div>
        
    </div>
    
</asp:Content>

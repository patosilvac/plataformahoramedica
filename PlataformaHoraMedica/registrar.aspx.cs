﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlataformaHoraMedica
{
    public partial class registrar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("index.aspx");
        }

        protected void btnReg_Click(object sender, EventArgs e)
        {
            string rut, nombre, apellido, comuna, direccion, numero, contrasegna, msjError="", resumen;


            rut = txtRutReg.Text;
            nombre = txtNombreReg.Text;
            apellido = txtApellidoReg.Text;
            comuna = ListadoComunasReg.SelectedItem.Text;
            direccion = txtDireccionReg.Text;
            numero = txtNumeroReg.Text;
            contrasegna = txtPassReg.Text;


            if (rut == "" || nombre == "" || apellido == "" || comuna == "Seleccionar" || direccion == "" || numero == "" || contrasegna == "")
            {
                msjError = "Faltan campos por llenar";
                labelError.Text = msjError;
            }
            else
            {
                msjError = "";
                labelError.Text = msjError;
                resumen = "RESUMEN DE REGISTRO:" + 
                    " Rut: " + rut + 
                    " Nombre: "+nombre+ 
                    " Apellido: " + apellido + 
                    " Comuna: " + comuna + 
                    " Direccion: " + direccion + 
                    " Numero: " + numero;
                labelResumen.Text = resumen;
            }
        }
    }
}
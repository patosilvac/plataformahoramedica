﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site2.Master" AutoEventWireup="true" CodeBehind="horas_disponibles.aspx.cs" Inherits="PlataformaHoraMedica.horas_disponibles" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="registro-form container-fluid col-4">
        <h3 class="text-center text-primary">Horas disponibles</h3>
        <div class="form-group">
            <asp:Label ID="LblRutHD" runat="server" Text="Rut paciente"></asp:Label>
            <asp:TextBox ID="txtRutHD" runat="server" CssClass="form-control" ReadOnly="True">15283505-1</asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblNombreHD" runat="server" Text="Nombre Paciente"></asp:Label>
            <asp:TextBox ID="txtNombreHD" runat="server" CssClass="form-control" ReadOnly="True">Patricio</asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblApellidoHD" runat="server" Text="Apellido Paciente"></asp:Label>
            <asp:TextBox ID="txtApellidoHD" runat="server" CssClass="form-control" ReadOnly="True">Silva</asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblMedicoHD" runat="server" Text="Medico"></asp:Label>
            <asp:TextBox ID="txtMedicoHD" runat="server" CssClass="form-control" ReadOnly="True">Dr. Silva</asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Label ID="LblEspeHD" runat="server" Text="Especialidad"></asp:Label>
            <asp:TextBox ID="txtEspeHD" runat="server" CssClass="form-control" ReadOnly="True">Neurologia</asp:TextBox>
        </div>
        <div class="form-group container">
            <div class="row">
                <div class="col-8">
                <asp:Label ID="LblFechaHD" runat="server" Text="Fecha"></asp:Label>
                <asp:Calendar ID="Calendar1" runat="server" BackColor="White" BorderColor="#999999" Font-Names="Verdana" Font-Size="8pt" 
                    ForeColor="Black" Height="180px" Width="100%" CellPadding="4" DayNameFormat="Shortest" >
                    <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                    <NextPrevStyle VerticalAlign="Bottom" />
                    <OtherMonthDayStyle ForeColor="#808080" />
                    <SelectedDayStyle BackColor="#666666" ForeColor="White" Font-Bold="True" />
                    <SelectorStyle BackColor="#CCCCCC" />
                    <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                    <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                    <WeekendDayStyle BackColor="#FFFFCC" />
                </asp:Calendar>
                </div>
                <div class="col-4">
                    <asp:Label ID="LblHorarioHD" runat="server" Text="Horario"></asp:Label>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" >
                        <asp:ListItem Value="1">9:00</asp:ListItem>
                        <asp:ListItem Value="2">9:30</asp:ListItem>
                        <asp:ListItem Value="3">10:00</asp:ListItem>
                        <asp:ListItem Value="4">10:30</asp:ListItem>
                        <asp:ListItem Value="5">11:00</asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </div>
        </div>
        <div class="form-group">
            <asp:Label ID="labelError" runat="server" Text="" CssClass="text-danger"></asp:Label>
        </div>
        <div class="form-group">
            <asp:Button ID="btnAgendar" runat="server" Text="Agendar Hora" CssClass="btn btn-primary btn-block" OnClick="Button1_Click" />
        </div>
        <div class="form-group">
            <asp:Button ID="btnVolverAge" runat="server" Text="Volver" CssClass="btn btn-warning btn-block" OnClick="Button2_Click" />
        </div>
    </div>
</asp:Content>

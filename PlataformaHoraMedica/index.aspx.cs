﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PlataformaHoraMedica
{
    public partial class index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            
            string rut, contrasegna, msjError = "";
            rut = TxtRut.Text;
            contrasegna = TxtPass.Text;

            if (rut == "" || contrasegna == "")
            {
                msjError = "Faltan campos por llenar";
                labelError.Text = msjError;
            }
            
            else
            {
                Response.Redirect("agendamiento.aspx");
            }

            
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            Response.Redirect("registrar.aspx");
        }

    }
}